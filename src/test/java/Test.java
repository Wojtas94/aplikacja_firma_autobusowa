import Model.Bus;
import Model.Driver;
import Model.IsAvailable;
import Model.Trip;
import Repository.BusRepository;
import Repository.DriverRepository;
import Repository.RepairRepository;
import Repository.TripRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class Test {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("BusAppPersistenceUnit");
    EntityManager entityManager = factory.createEntityManager();

    BusRepository busRepository = new BusRepository(entityManager);
    TripRepository tripRepository = new TripRepository(entityManager);
    RepairRepository repairRepository = new RepairRepository(entityManager);
    DriverRepository driverRepository = new DriverRepository(entityManager);

    @org.junit.Test
    public void testAddDriver_shouldReturnTrue(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").build();
        boolean b = driverRepository.create(driver);
        assertTrue(b);
    }
    @org.junit.Test
    public void testGetDriver(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").build();
        Driver driver1 = Driver.builder().name("Wojtek").surname("Wojciechowski").id(1L).build();
        driverRepository.create(driver);
        assertEquals(driver1,  driverRepository.read(1L));
    }

    @org.junit.Test
    public void testDeleteDriver_shouldReturnTrue(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").build();
        boolean b = driverRepository.create(driver);

        assertTrue(driverRepository.delete(1L));
    }

    @org.junit.Test
    public void testUpdateDriver_shouldReturnTrue(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").build();
        Driver driver1 = Driver.builder().name("Marek").surname("Wojciechowski").id(1L).build();
        driverRepository.create(driver);
        boolean update = driverRepository.update(driver1);
        assertTrue(update);
    }

    @org.junit.Test
    public void testShowAll_shouldReturnList(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").build();
        Driver driver1 = Driver.builder().name("Jan").surname("Janowksi").build();
        Driver driver2 = Driver.builder().name("Witek").surname("Witkowski").build();
        Driver driver3 = Driver.builder().name("Rafał").surname("Rafalski").build();
        driverRepository.create(driver);
        driverRepository.create(driver2);
        driverRepository.create(driver1);
        driverRepository.create(driver3);

        List list = driverRepository.findAll();
        System.out.println(list);;
    }

    @org.junit.Test
    public void testAddBus_shouldReturnTrue(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").build();
        Driver driver1 = Driver.builder().name("Jan").surname("Janowksi").build();
        driverRepository.create(driver);
        driverRepository.create(driver1);
        List<Driver> list = new ArrayList<>();
        list.add(driver);
        list.add(driver1);

        Bus bus = Bus.builder().name("Tupoleww").registrationNumber("kda11222").drivers(list).build();
        boolean b = busRepository.create(bus);
        assertTrue(b);
    }

    @org.junit.Test
    public void testDelete_shouldReturnTrue(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").isAvailable(IsAvailable.T).build();
        Driver driver1 = Driver.builder().name("Jan").surname("Janowksi").isAvailable(IsAvailable.T).build();
        driverRepository.create(driver);
        driverRepository.create(driver1);
        List<Driver> list = new ArrayList<>();
        list.add(driver);
        list.add(driver1);

        Bus bus = Bus.builder().name("Tupoleww").registrationNumber("kda11222").isAvailable(IsAvailable.T).drivers(list).build();
        Bus bus1 = Bus.builder().name("Iveco").registrationNumber("kda992").isAvailable(IsAvailable.F).drivers(list).build();
        busRepository.create(bus);
        busRepository.create(bus1);
        boolean delete = busRepository.delete(1L);
        assertTrue(delete);
    }

    @org.junit.Test
    public void showAllBuses_shouldReturnList(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").isAvailable(IsAvailable.T).build();
        Driver driver1 = Driver.builder().name("Jan").surname("Janowksi").isAvailable(IsAvailable.T).build();
        driverRepository.create(driver);
        driverRepository.create(driver1);
        List<Driver> list = new ArrayList<>();
        list.add(driver);
        list.add(driver1);

        Bus bus = Bus.builder().name("Tupoleww").registrationNumber("kda11222").isAvailable(IsAvailable.T).drivers(list).build();
        Bus bus1 = Bus.builder().name("Iveco").registrationNumber("kda992").isAvailable(IsAvailable.F).drivers(list).build();
        busRepository.create(bus);
        busRepository.create(bus1);
        List lista = busRepository.findAll();
        System.out.println(lista);
    }

    @org.junit.Test
    public void testAddTrip_shouldReturnTrue(){
        Driver driver = Driver.builder().name("Wojtek").surname("Wojciechowski").isAvailable(IsAvailable.T).build();
        Driver driver1 = Driver.builder().name("Jan").surname("Janowksi").isAvailable(IsAvailable.T).build();
        driverRepository.create(driver);
        driverRepository.create(driver1);
        List<Driver> list = new ArrayList<>();
        list.add(driver);
        list.add(driver1);

        Bus bus = Bus.builder().name("Tupoleww").registrationNumber("kda11222").isAvailable(IsAvailable.T).drivers(list).build();
        Bus bus1 = Bus.builder().name("Iveco").registrationNumber("kda992").isAvailable(IsAvailable.F).drivers(list).build();
        List<Bus> listofbuses = new ArrayList<>();
        listofbuses.add(bus);
        listofbuses.add(bus1);
        busRepository.create(bus);
        busRepository.create(bus1);
        Trip trip = Trip.builder().buses(listofbuses).startDate(LocalDateTime.of(2019, 5, 22, 17, 45).truncatedTo(ChronoUnit.SECONDS)).
                finishDate(LocalDateTime.of(2019, 5, 21,10, 0 ).truncatedTo(ChronoUnit.HOURS)).startPlace("Olesno").finishPlace("Kraków").price(2222).amountOfPassengers(22).build();

        boolean b = tripRepository.create(trip);
        assertTrue(b);
        System.out.println(trip);


    }
}
