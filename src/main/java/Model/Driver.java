package Model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "driver")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Getter
@Setter
@ToString
public class Driver implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_driver")
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(length = 2, nullable = false, name = "is_available")
    @Enumerated(EnumType.STRING)
    private IsAvailable isAvailable;

    @Column(nullable = false, length = 30)
    private String surname;


}
