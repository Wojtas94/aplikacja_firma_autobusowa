package Model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "trip")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Getter
@Setter
@ToString
public class Trip implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_trip")
    private Long id;

    @Column(nullable = false, name = "start_date")
    private LocalDateTime startDate;

    @Column(nullable = false, name = "finish_date")
    private LocalDateTime finishDate;

    @Column(length = 50, nullable = false, name = "start_place")
    private String startPlace;

    @Column(name = "amount_of_passengers")
    private Integer amountOfPassengers;

    private Integer price;

    private Integer kilometers;

    @Column(length = 50, nullable = false, name = "finish_place")
    private String finishPlace;

    private String notes;

    @ManyToMany
    @JoinTable(name = "trip_bus",
            joinColumns = {@JoinColumn(name = "trip_id", referencedColumnName = "id_trip")},
            inverseJoinColumns = {@JoinColumn(name = "bus_id", referencedColumnName = "id_bus")})
    private List<Bus> buses;
}
