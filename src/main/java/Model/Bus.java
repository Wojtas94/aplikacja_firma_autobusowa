package Model;

import lombok.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "bus")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
@Getter
@Setter
@ToString
@SpringBootApplication
@EnableScheduling
public class Bus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_bus")
    private Long id;

    @Column(unique = true, length = 10, name = "registration_number", nullable = false)
    private String registrationNumber;

    @Column(length = 30)
    private String name;

    @Column(length = 2, nullable = false, name = "is_available")
    @Enumerated(EnumType.STRING)
    private IsAvailable isAvailable;

    @ManyToMany
    @JoinTable(name = "bus_driver",
            joinColumns = {@JoinColumn(name = "bus_id", referencedColumnName = "id_bus")},
            inverseJoinColumns = {@JoinColumn(name = "driver_id", referencedColumnName = "id_driver")})
    private List<Driver> drivers;

    @OneToMany(mappedBy = "bus")
    private List<Repair> repairs;

    @ManyToMany(mappedBy = "buses")
    private List<Trip> trips;


}
