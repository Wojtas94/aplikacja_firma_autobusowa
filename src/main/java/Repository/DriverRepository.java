package Repository;

import Model.Driver;

import javax.persistence.EntityManager;

public class DriverRepository extends GenericDao<Driver, Long> {
    public DriverRepository(EntityManager entityManager) {
        super(entityManager);
    }
}
