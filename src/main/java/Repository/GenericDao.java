package Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericDao<T, K> {

    protected final EntityManager entityManager;
    protected final Class<T> entityClass;

    @SuppressWarnings("unchecked")
    public GenericDao(EntityManager entityManager){
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
        this.entityManager = entityManager;
    }

    public boolean create(T entity){
        EntityTransaction transaction = entityManager.getTransaction();
        try{
            transaction.begin();
            entityManager.persist(entity);
            transaction.commit();
            return true;
        }catch (Exception e){
            transaction.rollback();
            return false;
        }
    }

    public boolean update(T entity){
        EntityTransaction transaction = entityManager.getTransaction();
        try{
            transaction.begin();
            entityManager.merge(entity);
            transaction.commit();
            return true;
        }catch (Exception e){
            transaction.rollback();
            return false;
        }
    }

    public T read(K id){
       return entityManager.find(entityClass, id);
    }

    public boolean delete(K id){
        EntityTransaction transaction = entityManager.getTransaction();
        try{
            transaction.begin();
            T read = read(id);
            entityManager.remove(read);
            transaction.commit();
            return true;
        }catch (Exception e){
            transaction.rollback();
            return false;
        }
    }

    public List findAll(){
        List allObjects = new ArrayList();
        EntityTransaction transaction = entityManager.getTransaction();
        try{
            transaction.begin();
            allObjects = entityManager.createQuery("Select t from " + entityClass.getSimpleName() + " t").getResultList();
            transaction.commit();
            return allObjects;
        }catch (Exception e){
            transaction.rollback();
            return allObjects;
        }
    }
}
