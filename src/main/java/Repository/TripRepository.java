package Repository;

import Model.Trip;

import javax.persistence.EntityManager;

public class TripRepository extends GenericDao<Trip, Long> {
    public TripRepository(EntityManager entityManager) {
        super(entityManager);
    }
}
