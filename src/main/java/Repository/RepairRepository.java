package Repository;

import Model.Repair;

import javax.persistence.EntityManager;

public class RepairRepository extends GenericDao<Repair, Long> {
    public RepairRepository(EntityManager entityManager) {
        super(entityManager);
    }
}
