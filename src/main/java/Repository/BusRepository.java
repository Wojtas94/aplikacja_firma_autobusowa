package Repository;

import Model.Bus;

import javax.persistence.EntityManager;

public class BusRepository extends GenericDao<Bus, Long> {

    public BusRepository(EntityManager entityManager) {
        super(entityManager);
    }
}
